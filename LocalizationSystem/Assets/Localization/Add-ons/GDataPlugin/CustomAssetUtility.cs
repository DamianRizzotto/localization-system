///////////////////////////////////////////////////////////////////////////////
///
/// CustomAssetUtility.cs
/// 
/// (c)2013 Kim, Hyoun Woo
///
///////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEditor;
using System.IO;

public static class CustomAssetUtility
{
    public static string GetUniqueAssetPathNameOrFallback(string filename)
    {
        string path;
        try
        {
            // Private implementation of a file naming function which puts the file at the selected path.
            System.Type assetDatabase = typeof(AssetDatabase);
            path = (string)assetDatabase.GetMethod("GetUniquePathNameAtSelectedPath",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static)
                ?.Invoke(assetDatabase, new object[] { filename });
        }
        catch
        {
            // Protection against implementation changes.
            path = AssetDatabase.GenerateUniqueAssetPath("Assets/" + filename);
        }
        return path;
    }
}