using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

// to resolve TlsException error.
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

using Google.GData.Client;
using Google.GData.Spreadsheets;

namespace UnityQuickSheet
{
    /// <summary>
    /// Base class of .asset ScriptableObject class created from google spreadsheet.
    /// </summary>
    public class BaseGoogleEditor<T> : Editor where T : ScriptableObject
    {
        
        protected SerializedObject targetObject;
        protected SerializedProperty spreadsheetProp;
        protected SerializedProperty worksheetProp;
        protected SerializedProperty serializedData;

        protected GUIStyle box;
        protected bool isGUISkinInitialized = false;
        
        /// 
        /// Actively ignore security concerns to resolve TlsException error.
        /// 
        /// See: http://www.mono-project.com/UsingTrustedRootsRespectfully
        ///
        private static bool Validator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public void OnEnable()
        {
            T t = (T)target;
            targetObject = new SerializedObject(t);

            spreadsheetProp = targetObject.FindProperty("SheetName");
            if (spreadsheetProp == null)
                Debug.LogError("Failed to find 'SheetName' property.");

            worksheetProp = targetObject.FindProperty("WorksheetName");
            if (worksheetProp == null)
                Debug.LogError("Failed to find 'WorksheetName' property.");

            serializedData = targetObject.FindProperty("dataArray");
            if (serializedData == null)
                Debug.LogError("Failed to find 'dataArray' member field.");
            
            
            // resolves TlsException error
            ServicePointManager.ServerCertificateValidationCallback = Validator;

            GoogleDataSettings settings = GoogleDataSettings.Instance;
            if (settings != null)
            {
                if (string.IsNullOrEmpty(settings.OAuth2Data.client_id) ||
                    string.IsNullOrEmpty(settings.OAuth2Data.client_secret))
                    Debug.LogWarning("Client_ID and Client_Sceret is empty. Reload .json file.");

                if (string.IsNullOrEmpty(settings._AccessCode))
                    Debug.LogWarning("AccessCode is empty. Redo authenticate again.");
            }
            else
            {
                Debug.LogError("Failed to get google data settings. See the google data setting if it has correct path.");
                return;
            }
        }

        /// <summary>
        /// Draw Inspector view.
        /// </summary>
        public override void OnInspectorGUI()
        {
            if (target == null)
                return;

            // Update SerializedObject
            targetObject.Update();

            if (GUILayout.Button("Download"))
            {
                if (!Load())
                    Debug.LogError("Failed to Load data from Google.");
            }

            EditorGUILayout.Separator();

            DrawInspector();

            // Be sure to call [your serialized object].ApplyModifiedProperties()to save any changes.  
            targetObject.ApplyModifiedProperties();
        }
        
        /// <summary>
        /// Draw serialized properties on the Inspector view.
        /// </summary>
        /// <param name="useGUIStyle"></param>
        private void DrawInspector(bool useGUIStyle = true)
        {
            // Draw 'spreadsheet' and 'worksheet' name.
            EditorGUILayout.TextField(spreadsheetProp.name, spreadsheetProp.stringValue);
            EditorGUILayout.TextField(worksheetProp.name, worksheetProp.stringValue);

            // Draw properties of the data class.
            if (useGUIStyle && !isGUISkinInitialized)
            {
                isGUISkinInitialized = true;
                InitGUIStyle();
            }

            if (useGUIStyle)
                GUIHelper.DrawSerializedProperty(serializedData, box);
            else
                GUIHelper.DrawSerializedProperty(serializedData);
        }
        
        /// <summary>
        /// Create and initialize a new gui style which can be used for representing 
        /// each element of dataArray.
        /// </summary>
        private void InitGUIStyle()
        {
            box = new GUIStyle("box");
            box.normal.background = Resources.Load(EditorGUIUtility.isProSkin ? "brown" : "lightSkinBox", typeof(Texture2D)) as Texture2D;
            box.border = new RectOffset(4, 4, 4, 4);
            box.margin = new RectOffset(3, 3, 3, 3);
            box.padding = new RectOffset(4, 4, 4, 4);
        }
        
        /// <summary>
        /// Called when 'Update'(or 'Download' for google data) button is pressed. 
        /// It should be reimplemented in the derived class.
        /// </summary>
        protected virtual bool Load()
        {
            throw new NotImplementedException();
        }
    }
}