﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace UnityQuickSheet
{
    /// <summary>
    /// Resolve TlsException error.
    /// </summary>
    public class UnsafeSecurityPolicy
    {
        public static bool Validator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            //Debug.Log("Validation successful!");
            return true;
        }

        public static void Instate()
        {
            ServicePointManager.ServerCertificateValidationCallback = Validator;
        }
    }

    /// <summary>
    /// Editor script class for GoogleDataSettings scriptable object to hide password of google account.
    /// </summary>
    [CustomEditor(typeof(GoogleDataSettings))]
    public class GoogleDataSettingsEditor : Editor
    {
        public void OnEnable()
        {
            // resolve TlsException error
            UnsafeSecurityPolicy.Instate();
        }

        public override void OnInspectorGUI()
        {
            GUI.changed = false;
            const int labelWidth = 90;

            GUIStyle headerStyle = GUIHelper.MakeHeader();
            GUILayout.Label("GoogleSpreadsheet Settings", headerStyle);

            GoogleDataSettings.OAuth2JsonData oAuth2Data = GoogleDataSettings.Instance.OAuth2Data;

            if (string.IsNullOrEmpty(oAuth2Data.client_id) || string.IsNullOrEmpty(oAuth2Data.client_secret) || string.IsNullOrEmpty(GoogleDataSettings.Instance._AccessCode) || string.IsNullOrEmpty(GoogleDataSettings.Instance._RefreshToken)) {
                
                const string infoMsg = "Copying 'client_id' and 'client_secret' from Google Developer Console and pasting that into the text fields without specifying json file is also working, if you don't want to install oauth2 json file on the local disk.";
                GUIHelper.HelpBox(infoMsg, MessageType.Info);
                
                EditorGUILayout.Separator();
                GoogleDataSettings.useOAuth2JsonFile = GUILayout.Toggle(GoogleDataSettings.useOAuth2JsonFile, " I have OAuth2 JSON file");
                EditorGUILayout.Separator();

                if (GoogleDataSettings.useOAuth2JsonFile)
                {
                    GUILayout.BeginHorizontal(); // Begin json file setting
                    GUILayout.Label("JSON File:", GUILayout.Width(labelWidth));

                    string path = string.IsNullOrEmpty(GoogleDataSettings.Instance.JsonFilePath) ? Application.dataPath : GoogleDataSettings.Instance.JsonFilePath;

                    GoogleDataSettings.Instance.JsonFilePath = GUILayout.TextField(path, GUILayout.Width(250));
                    if (GUILayout.Button("...", GUILayout.Width(20)))
                    {
                        string folder = Path.GetDirectoryName(path);
                        path = EditorUtility.OpenFilePanel("Open JSON file", folder, "json");
                        if (path.Length != 0)
                        {
                            StringBuilder builder = new StringBuilder();
                            using (StreamReader sr = new StreamReader(path))
                            {
                                string s = "";
                                while (s != null)
                                {
                                    s = sr.ReadLine();
                                    builder.Append(s);
                                }
                            }

                            string jsonData = builder.ToString();

                            //HACK: reported a json file which has no "installed" property
                            //var oauthData = JObject.Parse(jsonData).SelectToken("installed").ToString();
                            //GoogleDataSettings.Instance.OAuth2Data = JsonConvert.DeserializeObject<GoogleDataSettings.OAuth2JsonData>(oauthData);

                            //HACK: assume the parsed json string contains only one property value: JObject.Parse(jsonData).Count == 1
                            JObject jo = JObject.Parse(jsonData);
                            var propertyValues = jo.PropertyValues();
                            foreach (JToken token in propertyValues)
                            {
                                string val = token.ToString();
                                GoogleDataSettings.Instance.OAuth2Data = JsonConvert.DeserializeObject<GoogleDataSettings.OAuth2JsonData>(val);
                            }

                            GoogleDataSettings.Instance.JsonFilePath = path;

                            // force to save the setting.
                            EditorUtility.SetDirty(GoogleDataSettings.Instance);
                            AssetDatabase.SaveAssets();
                        }
                    }
                    GUILayout.EndHorizontal(); // End json file setting.
                }
                
                oAuth2Data.client_id = oAuth2Data.client_id ?? string.Empty;
                oAuth2Data.client_secret = oAuth2Data.client_secret ?? string.Empty;
            
                // client_id for OAuth2
                GUILayout.BeginHorizontal();
                GUILayout.Label("Client ID: ", GUILayout.Width(labelWidth));
                oAuth2Data.client_id = GUILayout.TextField(oAuth2Data.client_id);
                GUILayout.EndHorizontal();

                // client_secret for OAuth2
                GUILayout.BeginHorizontal();
                GUILayout.Label("Client Secret: ", GUILayout.Width(labelWidth));
                oAuth2Data.client_secret = GUILayout.TextField(oAuth2Data.client_secret);
                GUILayout.EndHorizontal();


                if (GUILayout.Button("Start Authentication"))
                {
                    GDataDB.Impl.GDataDBRequestFactory.InitAuthenticate();
                }
                
                EditorGUILayout.Separator();

                // client_secret for Access Code
                GUILayout.BeginHorizontal();
                GUILayout.Label("AccessCode: ", GUILayout.Width(labelWidth));
                GoogleDataSettings.Instance._AccessCode = GUILayout.TextField(GoogleDataSettings.Instance._AccessCode);
                GUILayout.EndHorizontal();
                
                if (GUILayout.Button("Finish Authentication"))
                {
                    try
                    {
                        GDataDB.Impl.GDataDBRequestFactory.FinishAuthenticate();
                    }
                    catch (Exception e)
                    {
                        EditorUtility.DisplayDialog("Error", e.Message, "OK");
                    }
                }
                EditorGUILayout.Separator();
                
            } else {
                // client_id for OAuth2
                GUILayout.BeginHorizontal();
                GUILayout.Label("Client ID: ", GUILayout.Width(labelWidth));
                GUILayout.TextField(oAuth2Data.client_id);
                GUILayout.EndHorizontal();

                // client_secret for OAuth2
                GUILayout.BeginHorizontal();
                GUILayout.Label("Client Secret: ", GUILayout.Width(labelWidth));
                GUILayout.TextField(oAuth2Data.client_secret);
                GUILayout.EndHorizontal();

                // client_secret for Access Code
                GUILayout.BeginHorizontal();
                GUILayout.Label("AccessCode: ", GUILayout.Width(labelWidth));
                GUILayout.TextField(GoogleDataSettings.Instance._AccessCode);
                GUILayout.EndHorizontal();
                
                // reset client_id and client_secret and empty its text fields.
                if (GUILayout.Button("Reset"))
                {
                    GoogleDataSettings.Instance.OAuth2Data.client_id = string.Empty;
                    GoogleDataSettings.Instance.OAuth2Data.client_secret = string.Empty;
                    GoogleDataSettings.Instance._AccessCode = string.Empty;

                    // retrieves from google developer center.
                    GoogleDataSettings.Instance._RefreshToken = string.Empty;
                    GoogleDataSettings.Instance._AccessToken = string.Empty;
                }
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(GoogleDataSettings.Instance);
            }
        }
    }
}