using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

using GDataDB;
using GDataDB.Impl;
using Google.GData.Client;
using Google.GData.Spreadsheets;

namespace UnityQuickSheet {
    /// <summary>
    /// An editor script class of GoogleMachine class.
    /// </summary>
    [CustomEditor(typeof(GoogleMachine))]
    public class GoogleMachineEditor : Editor {
        private GoogleMachine machine;
        private DatabaseClient client;

        private Texture spreadsheetIcon;
        private Texture worksheetIcon;
        private GUIStyle headerStyle = null;
        private GUISkin skin = null;

        private List<string> optionsCache = new List<string>();
        
        protected void OnEnable() {
            // resolve TlsException error
            UnsafeSecurityPolicy.Instate();

            machine = target as GoogleMachine;
            client = new DatabaseClient("","");
            
            skin = EditorGUIUtility.Load("Assets/Localization/Scripts/Editor/LocalizationGUISkin.guiskin") as GUISkin;
            
            spreadsheetIcon = EditorGUIUtility.Load("Assets/Localization/Scripts/Editor/spreadsheet.png") as Texture;
            worksheetIcon = EditorGUIUtility.Load("Assets/Localization/Scripts/Editor/worksheet.png") as Texture;
        }

        /// <summary>
        /// Draw custom UI.
        /// </summary>
        public override void OnInspectorGUI() {
            if (skin != null) {
                GUI.skin = skin;
            }
            
            if (headerStyle == null) {
                headerStyle = GUIHelper.MakeHeader();
            }
            
            if (GoogleDataSettings.Instance == null) {
                GUILayout.BeginHorizontal();
                GUILayout.Toggle(true, "", "CN EntryError", GUILayout.Width(20));
                GUILayout.BeginVertical();
                GUILayout.Label("", GUILayout.Height(12));
                GUILayout.Label("Check the GoogleDataSetting.asset file exists or its path is correct.",GUILayout.Height(20));
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }

            GUILayout.Label("Google Spreadsheet Settings:", headerStyle);

            EditorGUILayout.Separator();

            if (string.IsNullOrEmpty(machine.SpreadsheetName)) {
                if (string.IsNullOrEmpty(GoogleDataSettings.Instance._AccessCode) || string.IsNullOrEmpty(GoogleDataSettings.Instance._RefreshToken)) {
                    GUIHelper.HelpBox("It seems like OAuth2 authentication has failed or hasn't been properly set. Please check out the \'Google Data Oauth2 Settings\' file ", MessageType.Error);
                } else {
                    machine.WorkSheetName = string.Empty;
                
                    GUILayout.Label("Spreadsheet name:");

                    GUILayout.BeginHorizontal();

                    GUILayout.BeginScrollView(Vector2.zero, false, false);

                    if (optionsCache.Count == 0) {
                        if (client != null) {
                            foreach (AtomEntry entry in client.GetDatabaseEntries().OrderByDescending(e => e.Updated)) {
                                optionsCache.Add(entry.Content.Content);
                            }
                        }
                    }

                    for (var index = 0; index < optionsCache.Count; index++) {
                        string sheetName = optionsCache[index];
                        GUILayout.BeginHorizontal();
                        if (GUILayout.Button(sheetName)) {
                            machine.SpreadsheetName = sheetName;
                            optionsCache.Clear();
                        }
                        GUI.Label(new Rect(20, 7 + index * 47, 70, 70), new GUIContent(spreadsheetIcon));
                        GUILayout.EndHorizontal();
                    }

                    GUILayout.EndScrollView();

                    GUILayout.EndHorizontal();
                }
            } else if (string.IsNullOrEmpty(machine.WorkSheetName)) {
                GUILayout.Label("Worksheet name:");

                GUILayout.BeginHorizontal();

                GUILayout.BeginScrollView(Vector2.zero, false, false);

                if (optionsCache.Count == 0) {
                    if (client != null) {
                        string error = string.Empty;
                        Database db = (Database) client.GetDatabase(machine.SpreadsheetName, ref error);
                        if (db != null) {
                            foreach (WorksheetEntry entry in db.GetWorksheetEntries()) {
                                optionsCache.Add(entry.Title.Text);
                            }
                        } else {
                            Debug.LogError($"Error downloading spreadsheet [{machine.SpreadsheetName}] info: {error}");
                        }
                    }
                }

                for (var index = 0; index < optionsCache.Count; index++) {
                    string sheetName = optionsCache[index];
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button(sheetName)) {
                        machine.WorkSheetName = sheetName;
                        optionsCache.Clear();
                    }
                    GUI.Label(new Rect(20, 7 + index * 47, 70, 70), new GUIContent(worksheetIcon));
                    GUILayout.EndHorizontal();
                }

                GUILayout.EndScrollView();

                GUILayout.EndHorizontal();
            } else {
                machine.SpreadsheetName = EditorGUILayout.TextField("SpreadSheet Name: ", machine.SpreadsheetName);
                machine.WorkSheetName = EditorGUILayout.TextField("WorkSheet Name: ", machine.WorkSheetName);

                EditorGUILayout.Separator();

                if (GUILayout.Button("Import")) {
                    Import();
                }
                
                EditorGUILayout.Separator();

                EditorGUILayout.LabelField($"Last Updated: {machine.LastUpdated}");

                if (machine.Table != null) {
                    EditorGUILayout.LabelField($"Amount of rows: {machine.Table.GetLength(0)}");
                } else {
                    EditorGUILayout.LabelField($"Table is null");
                }
            }

            // force save changed type.
            if (GUI.changed) {
                EditorUtility.SetDirty(GoogleDataSettings.Instance);
                EditorUtility.SetDirty(machine);
            }
        }

        /// <summary>
        /// Connect to the google spreadsheet and retrieves its header columns.
        /// </summary>
        private void Import() {
            if (string.IsNullOrEmpty(GoogleDataSettings.Instance._AccessCode) || string.IsNullOrEmpty(GoogleDataSettings.Instance._RefreshToken)) {
                Debug.LogError("Import fail. It seems like OAuth2 authentication has failed or hasn't been properly set. Please check out the \'Google Data Oauth2 Settings\' file ");
            }

            machine.Import();
            AssetDatabase.SaveAssets();
        }
    }
}