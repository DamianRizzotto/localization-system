using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace UnityQuickSheet
{
    /// <summary>
    /// A class manages google account setting.
    /// </summary>
    [CreateAssetMenu(menuName = "Localization/GoogleData Authentication Settings")]
    public class GoogleDataSettings : SingletonScriptableObject<GoogleDataSettings>
    {
        [System.Serializable]
        public struct OAuth2JsonData
        {
            public string client_id;
            public string auth_uri;
            public string token_uri;
            public string auth_provider_x509_cert_url;
            public string client_secret;
            public List<string> redirect_uris;
        };
        
        // A flag which indicates use local installed oauth2 json file for authentication or not.
        public static bool useOAuth2JsonFile = false;

        public string JsonFilePath
        {
            get => jsonFilePath;
            set
            {
                if (string.IsNullOrEmpty(value) == false) {
                    jsonFilePath = value;
                }
            }
        }
        private string jsonFilePath = string.Empty;

        // enter Access Code after getting it from auth url
        public string _AccessCode = "Paste Access Code here";

        public OAuth2JsonData OAuth2Data;
        
        // enter Auth 2.0 Refresh Token and AccessToken after succesfully authorizing with Access Code
        public string _RefreshToken = "";
        public string _AccessToken = "";

        /// <summary>
        /// Select currently exist account setting asset file.
        /// </summary>
        [MenuItem("Edit/Project Settings/QuickSheet/Select Google Data Setting")]
        public static void Edit()
        {
            Selection.activeObject = Instance;
            if (Selection.activeObject == null)
            {
                Debug.LogError("No GoogleDataSettings.asset file is found. Create setting file first.");
            }
        }
    }
}