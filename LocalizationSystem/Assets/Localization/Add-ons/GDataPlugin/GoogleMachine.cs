﻿using UnityEngine;
using UnityEditor;
using GDataDB;
using GDataDB.Impl;
using Google.GData.Spreadsheets;
using System;

namespace UnityQuickSheet
{
    /// <summary>
    /// A class for various setting to import google spreadsheet data and generated related script files.
    /// </summary>
    public class GoogleMachine : ScriptableObject, ISerializationCallbackReceiver 
    {
        private static readonly string ImportSettingFilename = "New Import Setting.asset";

        [SerializeField]
        private string spreadsheetName;
        public string SpreadsheetName
        {
            get => spreadsheetName;
            set => spreadsheetName = value;
        }

        [SerializeField]
        private string workSheetName;
        public string WorkSheetName
        {
            get => workSheetName;
            set => workSheetName = value;
        }
        
        [SerializeField]
        private DateTime lastUpdated;
        public DateTime LastUpdated
        {
            get => lastUpdated;
            private set => lastUpdated = value;
        }
        
        [SerializeField]
        private string[,] table;
        public string[,] Table
        {
            get => table;
            private set => table = value;
        }

        [SerializeField]
        public string lastUpdatedSerialization;
        
        [SerializeField]
        public string tableSerialization;
        
        /// <summary>
        /// A menu item which create a 'GoogleMachine' asset file.
        /// </summary>
        [MenuItem("Assets/Create/Localization/Google spreadsheet import settings")]
        public static void CreateGoogleMachineAsset()
        {
            GoogleMachine inst = CreateInstance<GoogleMachine>();
            string path = CustomAssetUtility.GetUniqueAssetPathNameOrFallback(ImportSettingFilename);
            AssetDatabase.CreateAsset(inst, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = inst;
        }

        public void Import() {
            Database db = GetDatabase();
            var worksheet = db.GetWorksheetEntry(WorkSheetName);

            if (worksheet == null) {
                Debug.LogError($"Worksheet was null for {SpreadsheetName}- {WorkSheetName}");
                return;
            }

            LastUpdated = worksheet.Updated;
            
            CellFeed cellFeed = worksheet.QueryCellFeed(ReturnEmptyCells.no);

            int maxRow = 0;
            foreach (CellEntry entry in cellFeed.Entries) {
                maxRow = Mathf.Max(maxRow, (int) entry.Row);
            }
            
            string[,] database = new string[maxRow, worksheet.Cols];
            foreach (var atomEntry in cellFeed.Entries) {
                var cell = (CellEntry) atomEntry;
                database[cell.Row - 1, cell.Column - 1] = cell.Value;
            }

            Table = database;
        }

        private Database GetDatabase() {
            // first we need to connect to the google-spreadsheet to get all the first row of the cells
            // which are used for the properties of data class.
            DatabaseClient client = new DatabaseClient("", "");

            if (string.IsNullOrEmpty(SpreadsheetName)) {
                Debug.LogError($"Spreadsheet name is empty");
                return null;
            }

            if (string.IsNullOrEmpty(WorkSheetName)) {
                Debug.LogError($"WorkSheet name is empty");
                return null;
            }

            string error = string.Empty;

            var db = client.GetDatabase(SpreadsheetName, ref error);
            if (db == null) {
                var message = string.IsNullOrEmpty(error) ? @"Unknown error." : $@"{error}";
                message += "\nSee 'GoogleDataSettings.asset' file and check the oAuth2 setting is correctly done.";
                EditorUtility.DisplayDialog("Error", message, "OK");
                return null;
            }

            return (Database) db;
        }

        public void OnBeforeSerialize() {
            tableSerialization = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            lastUpdatedSerialization = Newtonsoft.Json.JsonConvert.SerializeObject(lastUpdated);
        }

        public void OnAfterDeserialize() {
            table = Newtonsoft.Json.JsonConvert.DeserializeObject<string[,]>(tableSerialization);
            lastUpdated = Newtonsoft.Json.JsonConvert.DeserializeObject<DateTime>(lastUpdatedSerialization);
        }
        
    }
}