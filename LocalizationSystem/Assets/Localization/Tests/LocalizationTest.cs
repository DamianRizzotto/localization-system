﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TMPro;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Localization.Tests {
    public class LocalizationTest {
        
        [Test]
        public void SceneLocalizationPasses() {
            Dictionary<string,List<string>> missingLocalizationData = new Dictionary<string, List<string>>();
            for (int i = 0; i < SceneManager.sceneCount; i++) {
                Scene scene = SceneManager.GetSceneAt(i);
                EditorSceneManager.OpenScene(scene.path);

                List<string> missingLocalizationLabels = new List<string>();
                
                List<TextMeshProUGUI> labels = FindObjectsOfTypeAll<TextMeshProUGUI>();
                foreach (TextMeshProUGUI label in labels) {
                    if (label.GetComponent<LocalizedTextMeshPro>() == null && !label.CompareTag(TextLocalizationCheck.UNLOCALIZABLE_TAG)) {
                        missingLocalizationLabels.Add(label.name);
                    }
                }

                if (missingLocalizationLabels.Count != 0) {
                    missingLocalizationData[EditorSceneManager.GetActiveScene().name] = missingLocalizationLabels;
                }
            }

            string errorMessage = $"Missing localization items: \n{string.Join("\n", missingLocalizationData.Select( mld => $"Scene {mld.Key} : {string.Join(",", mld.Value)}"))}";
            Assert.IsEmpty(missingLocalizationData, errorMessage);
        }

        private static List<T> FindObjectsOfTypeAll<T>() {
            List<T> results = new List<T>();
            var s = SceneManager.GetSceneAt(0);
            var allGameObjects = s.GetRootGameObjects();
            foreach (var go in allGameObjects) {
                results.AddRange(go.GetComponentsInChildren<T>(true));
            }

            return results;
        }

        [Test]
        public void PrefabLocalizationPasses() {
            Dictionary<GameObject,List<string>> missingLocalizationData = new Dictionary<GameObject, List<string>>();

            string[] allPrefabs = GetAllPrefabs();
            foreach ( string prefab in allPrefabs ) {
                Object o = AssetDatabase.LoadMainAssetAtPath( prefab );
                try {
                    var go = (GameObject) o;
                    List<string> missingLocalizationLabels = new List<string>();

                    TextMeshProUGUI[] labels = go.GetComponentsInChildren<TextMeshProUGUI>(true);
                    foreach (TextMeshProUGUI label in labels) {
                        if (label.GetComponent<LocalizedTextMeshPro>() == null && !label.CompareTag(TextLocalizationCheck.UNLOCALIZABLE_TAG)) {
                            missingLocalizationLabels.Add(label.name);
                        }
                    }
                    
                    if (missingLocalizationLabels.Count != 0) {
                        missingLocalizationData[go] = missingLocalizationLabels;
                    }
                } catch {
                    Debug.Log( "For some reason, prefab " + prefab + " won't cast to GameObject" );
                }
            }
            
            string errorMessage = $"Missing localization items: \n{string.Join("\n", missingLocalizationData.Select( mld => $"GameObject {mld.Key.name} : {string.Join(",", mld.Value)}"))}";
            Assert.IsEmpty(missingLocalizationData, errorMessage);
        }

        private static string[] GetAllPrefabs() {
            string[] temp = AssetDatabase.GetAllAssetPaths();
            List<string> result = new List<string>();
            foreach (string s in temp) {
                if (s.Contains(".prefab")) result.Add(s);
            }

            return result.ToArray();
        }
    }
}