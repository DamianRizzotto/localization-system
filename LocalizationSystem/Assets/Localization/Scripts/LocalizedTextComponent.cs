﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Localization {
    public abstract class LocalizedTextComponent<T> : MonoBehaviour, ILocalizable where T : Component {
        [Tooltip("The text component to localize")] [SerializeField]
        protected T text;

        [Tooltip("The key to localize with")] [SerializeField]
        private string key;

        public List<object> Parameters { get; } = new List<object>();

        public void Reset() {
            text = GetComponent<T>();
        }

        public void OnEnable() {
            Localization.Instance.AddOnLocalizeEvent(this);
        }

        public void SetKey(string key) {
            this.key = key;
            OnLocalize();
        }

        public string GetKey() {
            return key;
        }

        protected abstract void SetText(T component, string value);

        public void OnLocalize() {
#if UNITY_EDITOR
            var flags = text != null ? text.hideFlags : HideFlags.None;
            if (text != null) text.hideFlags = HideFlags.DontSave;
#endif
            if (Enum.TryParse(key, out LocKey locKey)) {
                if (Parameters != null && Parameters.Count > 0) {
                    SetText(text, Localization.GetFormat(locKey, Parameters.ToArray()));
                } else {
                    SetText(text, Localization.LocalizeKey(locKey));
                }
            } else {
                SetText(text, Localization.GetKeyNoFoundString(key));
            }
#if UNITY_EDITOR
            if (text != null) text.hideFlags = flags;
#endif
        }

        public void ClearParameters() {
            Parameters.Clear();
        }

        public void AddParameter(object parameter) {
            Parameters.Add(parameter);
            OnLocalize();
        }

        public void AddParameter(int parameter) {
            AddParameter((object) parameter);
        }

        public void AddParameter(float parameter) {
            AddParameter((object) parameter);
        }

        public void AddParameter(string parameter) {
            AddParameter((object) parameter);
        }
    }
}