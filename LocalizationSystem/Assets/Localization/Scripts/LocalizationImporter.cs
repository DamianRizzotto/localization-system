﻿using System;
using System.Collections.Generic;
using System.Linq;
using GDataDB;
using Google.GData.Spreadsheets;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityQuickSheet;

namespace Localization {
    [Serializable]
    public enum LocalizationAssetFormat {
        CSV,
        TSV
    }

    [Serializable]
    public class LocalizationAsset {
        [SerializeField] private TextAsset textAsset;
        [SerializeField] private LocalizationAssetFormat format = LocalizationAssetFormat.CSV;

        public TextAsset TextAsset => textAsset;
        public LocalizationAssetFormat Format => format;
    }

    public static class LocalizationImporter {
        /// <summary>
        /// A dictionary with the key of the text item you want and a list of all the languages.
        /// </summary>
        private static readonly List<List<string>> LanguageStrings = new List<List<string>>();

        private static readonly List<string> LanguageKeys = new List<string>();

        private static readonly List<string> EmptyList = new List<string>();

        private static readonly List<GoogleMachine> InputFiles = new List<GoogleMachine>();

        static LocalizationImporter() {
            var settings = Localization.Instance;
            if (settings == null) {
                Debug.LogError("Could not find a Localization Settings file in Resources.");
                return;
            }

            Init(settings.InputFiles);
        }

        private static void Init(List<GoogleMachine> files) {
            InputFiles.Clear();
            InputFiles.AddRange(files);
        }

        public static void PopulateLanguageStrings() {
            LanguageStrings.Clear();
            LanguageKeys.Clear();
            
            for (var index = 0; index < InputFiles.Count; index++) {
                var inputAsset = InputFiles[index];
                if (inputAsset == null) {
                    Debug.LogError($"Files[{index}] is null");
                    continue;
                }

                string[,] table = inputAsset.Table;

                if (table == null) {
                    Debug.LogError($"Table is null for {inputAsset.SpreadsheetName}-{inputAsset.WorkSheetName}");
                    continue;
                }

                var canBegin = false;

                for (var i = 0; i < table.GetLength(0); i++){
                    var key = table[i,0];

                    if (!canBegin) {
                        if (key.Equals("START LOCALIZATION")) {
                            canBegin = true;
                        }
                        continue;
                    }

                    if (key.StartsWith("SECTION:")) {
                        continue;
                    }

                    if (string.IsNullOrEmpty(key)) {
                        //Ignore empty lines in the sheet
                        continue;
                    }

                    if (LanguageKeys.Contains(key)) {
                        continue;
                    }

                    LanguageKeys.Add(key);
                    
                    List<string> localizedStrings = new List<string>();
                    for (var j = 1; j < table.GetLength(1); j++) {
                        localizedStrings.Add(table[i,j]);
                    }
                    LanguageStrings.Add(localizedStrings);
                }
            }
        }

        /// <summary>
        /// Checks if the current string is \r or \n
        /// </summary>
        /// <param name="currentString"></param>
        /// <returns></returns>
        public static bool IsLineBreak(string currentString) {
            return currentString.Length == 1 && (currentString[0] == '\r' || currentString[0] == '\n')
                   || currentString.Length == 2 && currentString.Equals(Environment.NewLine);
        }

        /// <summary>
        /// Get all language strings on a given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static List<string> GetLanguages(LocKey key, List<Language> supportedLanguages = null) {
            if (LanguageStrings == null || LanguageStrings.Count == 0) {
                PopulateLanguageStrings();
            }

            if (!LanguageKeys.Contains(key.ToString())) {
                return EmptyList;
            }

            if (supportedLanguages == null || supportedLanguages.Count == 0) {
                return LanguageStrings[(int) key];
            }

            // Filter the supported languages down to the supported ones
            var supportedLanguageStrings = new List<string>(supportedLanguages.Count);
            foreach (var language in supportedLanguages) {
                var supportedLanguage = (int) language;
                supportedLanguageStrings.Add(LanguageStrings[(int) key][supportedLanguage]);
            }

            return supportedLanguageStrings;
        }

        public static Dictionary<string, List<string>> GetLanguagesStartsWith(string key) {
            if (LanguageStrings == null || LanguageStrings.Count == 0) {
                PopulateLanguageStrings();
            }

            var multipleLanguageStrings = new Dictionary<string, List<string>>();
            if (!string.IsNullOrEmpty(key)) {
                for (var index = 0; index < LanguageKeys.Count; index++) {
                    var languageKey = LanguageKeys[index];
                    if (languageKey.ToLower().StartsWith(key)) {
                        multipleLanguageStrings.Add(languageKey, LanguageStrings[index]);
                    }
                }
            }

            return multipleLanguageStrings;
        }

        public static Dictionary<string, List<string>> GetLanguagesContains(string key) {
            if (LanguageStrings == null || LanguageStrings.Count == 0) {
                PopulateLanguageStrings();
            }

            var multipleLanguageStrings = new Dictionary<string, List<string>>();
            if (!string.IsNullOrEmpty(key)) {
                for (var index = 0; index < LanguageKeys.Count; index++) {
                    var languageKey = LanguageKeys[index];
                    if (languageKey.ToLower().Contains(key)) {
                        multipleLanguageStrings.Add(languageKey, LanguageStrings[index]);
                    }
                }
            }

            return multipleLanguageStrings;
        }

        public static void Refresh() {
            LanguageStrings.Clear();
            PopulateLanguageStrings();
        }

        public static List<string> GetKeys() {
            return LanguageKeys.ToList();
        }
    }
}