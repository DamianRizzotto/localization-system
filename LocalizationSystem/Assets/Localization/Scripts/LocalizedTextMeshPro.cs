﻿using TMPro;
using UnityEngine;

namespace Localization {
    [AddComponentMenu("UI/Localized TextMesh Pro", 13)]
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedTextMeshPro : LocalizedTextComponent<TextMeshProUGUI> {
        protected override void SetText(TextMeshProUGUI text, string value) {
            text.text = value;
        }
    }
}