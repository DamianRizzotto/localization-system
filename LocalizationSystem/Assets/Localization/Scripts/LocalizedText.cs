﻿using UnityEngine;
using UnityEngine.UI;

namespace Localization {
    [AddComponentMenu("UI/Localized Text", 11)]
    [RequireComponent(typeof(Text))]
    public class LocalizedText : LocalizedTextComponent<Text> {
        protected override void SetText(Text text, string value) {
            if (text == null) {
                Debug.LogWarning("Missing text component for " + this, this);
                return;
            }

            text.text = value;
        }
    }
}