﻿using UnityEditor;
using UnityEngine;

namespace Localization {
    public class SaveLanguagePreference : MonoBehaviour, ILocalizable {
        private const string SAVED_LOCALIZATION_LANGUAGE = "SavedLocalizationLanguage";

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize() {
            GameObject go = new GameObject("SaveLanguagePreference");
            go.AddComponent<SaveLanguagePreference>();
        }

        private void Awake() {
            DontDestroyOnLoad(gameObject);
        }

        private void Start() {
            Language language;
            if (!PlayerPrefs.HasKey(SAVED_LOCALIZATION_LANGUAGE)) {
                language = GetDefaultLanguage();
            } else {
                language = (Language) PlayerPrefs.GetInt(SAVED_LOCALIZATION_LANGUAGE, (int) Language.EN);
            }

            Localization.Instance.SelectLanguage(language);
            Localization.Instance.AddOnLocalizeEvent(this);
        }

        private Language GetDefaultLanguage() {
            switch (Application.systemLanguage) {
                case SystemLanguage.Spanish:
                    return Language.ES;
                case SystemLanguage.French:
                    return Language.FR;
                case SystemLanguage.Italian:
                    return Language.IT;
                case SystemLanguage.Arabic:
                    return Language.AR;
                case SystemLanguage.German:
                    return Language.GR;
                case SystemLanguage.Japanese:
                    return Language.JP;
                case SystemLanguage.Korean:
                    return Language.KR;
                case SystemLanguage.Portuguese:
                    return Language.PT;
                case SystemLanguage.Russian:
                    return Language.RU;
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                case SystemLanguage.Chinese:
                    return Language.CH;
                default:
                    return Language.EN;
            }
        }

        public void OnLocalize() {
            PlayerPrefs.SetInt(SAVED_LOCALIZATION_LANGUAGE, (int) Localization.Instance.SelectedLanguage);
            PlayerPrefs.Save();
        }
    }
}