﻿using System;
using UnityEngine;

namespace Localization {
    [AddComponentMenu("Mesh/Localized TextMesh")]
    [RequireComponent(typeof(TextMesh))]
    public class LocalizedTextMesh : MonoBehaviour, ILocalizable {
        [Tooltip("The TextMesh component to localize")] [SerializeField]
        private TextMesh text;

        [Tooltip("The key to localize with")] [SerializeField]
        private string key;

        public void Reset() {
            text = GetComponent<TextMesh>();
        }

        public void Start() {
            Localization.Instance.AddOnLocalizeEvent(this);
        }

        public void OnLocalize() {
            var flags = text.hideFlags;
            text.hideFlags = HideFlags.DontSave;
            text.text = Enum.TryParse(key, out LocKey locKey)
                ? Localization.LocalizeKey(locKey)
                : Localization.GetKeyNoFoundString(key);
            text.hideFlags = flags;
        }
    }
}