﻿namespace Localization {
    public interface ILocalizable {
        void OnLocalize();
    }
}
