﻿using UnityEditor;

namespace Localization {
    public class LocalizationPostProcessor : AssetPostprocessor {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths) {
            foreach (var str in importedAssets) {
                if (str.EndsWith(".csv") && str.Contains("Localization")) {
                    LocalizationImporter.Refresh();
                }
            }
        }
    }
}