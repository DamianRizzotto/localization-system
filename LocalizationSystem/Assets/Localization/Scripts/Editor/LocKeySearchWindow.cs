﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Localization {
    public class LocKeySearchWindow : EditorWindow {
        private string searchKey;
        private Vector2 scroll;
        private AnimBool showAutoComplete;

        [MenuItem("Localization/Utils/Search Loc Key")]
        public static void ShowWindow() {
            LocKeySearchWindow window = (LocKeySearchWindow) GetWindow(typeof(LocKeySearchWindow));
            window.Show();
        }

        public virtual void OnEnable() {
            showAutoComplete = new AnimBool(true);
            showAutoComplete.valueChanged.AddListener(Repaint);
        }

        private void OnGUI() {
            searchKey = EditorGUILayout.TextField(searchKey);
            DrawAutoComplete();

            if (GUILayout.Button("Search")) {
                bool found = false;

                // get root objects in scene
                List<GameObject> allObjects = GetAllObjectsInScene();

                foreach (var go in allObjects) {
                    LocalizedTextMeshPro localizedTextMeshPro = go.GetComponent<LocalizedTextMeshPro>();
                    if (localizedTextMeshPro != null && String.Equals(localizedTextMeshPro.GetKey(), searchKey,
                            StringComparison.CurrentCultureIgnoreCase)) {
                        Debug.Log($"Found Loc Key in gameObject: {localizedTextMeshPro.gameObject.name}");
                        Selection.activeGameObject = go;
                        found = true;
                    }
                }

                if (!found) {
                    Debug.Log($"Loc Key {searchKey} not used");
                }
            }
        }

        private void DrawAutoComplete() {
            var localizedStrings = LocalizationImporter.GetLanguagesStartsWith(searchKey);

            if (localizedStrings.Count == 0) {
                localizedStrings = LocalizationImporter.GetLanguagesContains(searchKey);
            }

            var selectedLanguage = (int) Localization.Instance.SelectedLanguage;

            showAutoComplete.target = EditorGUILayout.Foldout(showAutoComplete.target, "Auto-Complete");
            if (EditorGUILayout.BeginFadeGroup(showAutoComplete.faded)) {
                EditorGUI.indentLevel++;

                var height = EditorGUIUtility.singleLineHeight * (Mathf.Min(localizedStrings.Count, 15) + 2);
                scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.Height(height));
                foreach (var local in localizedStrings) {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel(local.Key);
                    if (GUILayout.Button(local.Value[selectedLanguage], "CN CountBadge")) {
                        searchKey = local.Key;
                        GUIUtility.hotControl = 0;
                        GUIUtility.keyboardControl = 0;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndScrollView();
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();
        }

        private static List<GameObject> GetAllObjectsInScene() {
            List<GameObject> objectsInScene = new List<GameObject>();

            foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[]) {
                if (go.hideFlags != HideFlags.None)
                    continue;

                if (PrefabUtility.GetPrefabType(go) == PrefabType.Prefab ||
                    PrefabUtility.GetPrefabType(go) == PrefabType.ModelPrefab)
                    continue;

                objectsInScene.Add(go);
            }

            return objectsInScene;
        }
    }
}