﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor.AnimatedValues;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Localization {
    public sealed class TextLocalizationCheck : EditorWindow {
        private int _index;
        private List<GameObject> _gameObjects;
        private string _locKeyString = string.Empty;

        public const string UNLOCALIZABLE_TAG = "UnlocalizableText";

        [MenuItem("Localization/Utils/Check unlocalized TMProUGUI")]
        private static void Init() {
            TextLocalizationCheck window = (TextLocalizationCheck) GetWindow(typeof(TextLocalizationCheck));
            window.Refresh();
            window.Show();
        }

        private void OnGUI() {
            GUILayout.Label("Search for TextMeshProUGUI with no Localization script",
                EditorStyles.centeredGreyMiniLabel);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("◀", EditorStyles.miniButtonLeft, GUILayout.Width(100))) {
                Previous();
            }

            GUILayout.Label(_index + " / " + _gameObjects.Count, EditorStyles.miniButtonMid, GUILayout.Width(100));
            if (GUILayout.Button("▶", EditorStyles.miniButtonRight, GUILayout.Width(100))) {
                Next();
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            try {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Refresh", GUILayout.Width(300))) {
                    Refresh();
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            } catch (Exception e) {
                Debug.LogError(e);
            }

            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Set Unlocalizable");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Set Unlocalizable", GUILayout.Width(150))) {
                SetLocalizable(false);
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Add LocalizedTextMeshProComponent");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            _locKeyString = EditorGUILayout.TextField(_locKeyString);
            DrawAutoComplete();
            if (GUILayout.Button("Add Component")) {
                if (Localization.KeyExist(_locKeyString) && LocalizationImporter
                        .GetLanguages((LocKey) Enum.Parse(typeof(LocKey), _locKeyString)).Count != 0) {
                    SetLocalizable(true);
                    LocalizedTextMeshPro component = _gameObjects[_index].AddComponent<LocalizedTextMeshPro>();
                    component.SetKey(_locKeyString);
                    _locKeyString = string.Empty;
                    Refresh();
                }
            }

            //Keyboard events
            if (Event.current != null && Event.current.isKey && Event.current.isKey &&
                Event.current.type == EventType.KeyDown) {
                switch (Event.current.keyCode) {
                    case KeyCode.RightArrow:
                        Next();
                        break;
                    case KeyCode.LeftArrow:
                        Previous();
                        break;
                    case KeyCode.UpArrow:
                        SetLocalizable(true);
                        break;
                    case KeyCode.DownArrow:
                        SetLocalizable(false);
                        break;
                }
            }
        }

        private void Next() {
            _index = NumberUtils.Mod((_index + 1), _gameObjects.Count);
            HighlightObject();
        }

        private void Previous() {
            _index = NumberUtils.Mod((_index - 1), _gameObjects.Count);
            HighlightObject();
        }

        private void SetLocalizable(bool localizable) {
            GameObject go = _gameObjects[_index];
            go.tag = (localizable) ? "Untagged" : UNLOCALIZABLE_TAG;
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        private void Refresh() {
            List<TextMeshProUGUI> texts = FindObjectsOfTypeAll<TextMeshProUGUI>();
            _gameObjects = texts
                .Where(x => x.GetComponent<LocalizedTextMeshPro>() == null && !x.CompareTag(UNLOCALIZABLE_TAG))
                .Select(x => x.gameObject).ToList();
            _index = 0;
            HighlightObject();
        }

        private void HighlightObject() {
            if (_index >= 0 && _index < _gameObjects.Count) {
                Selection.activeGameObject = _gameObjects[_index];
                EditorGUIUtility.PingObject(_gameObjects[_index]);
            }
        }

        private static List<T> FindObjectsOfTypeAll<T>() {
            List<T> results = new List<T>();
            var s = EditorSceneManager.GetSceneAt(0);
            var allGameObjects = s.GetRootGameObjects();
            for (int j = 0; j < allGameObjects.Length; j++) {
                var go = allGameObjects[j];
                results.AddRange(go.GetComponentsInChildren<T>(true));
            }

            return results;
        }

        private Vector2 scroll;
        private AnimBool showAutoComplete;

        public void OnEnable() {
            showAutoComplete = new AnimBool(true);
            showAutoComplete.valueChanged.AddListener(Repaint);

            AddUnlocalizableTextTagIfNeeded();
        }

        private void DrawAutoComplete() {
            var localizedStrings = LocalizationImporter.GetLanguagesStartsWith(_locKeyString);

            if (localizedStrings.Count == 0) {
                localizedStrings = LocalizationImporter.GetLanguagesContains(_locKeyString);
            }

            var selectedLanguage = (int) Localization.Instance.SelectedLanguage;

            showAutoComplete.target = EditorGUILayout.Foldout(showAutoComplete.target, "Auto-Complete");
            if (EditorGUILayout.BeginFadeGroup(showAutoComplete.faded)) {
                EditorGUI.indentLevel++;

                var height = EditorGUIUtility.singleLineHeight * (Mathf.Min(localizedStrings.Count, 15) + 2);
                scroll = EditorGUILayout.BeginScrollView(scroll, GUILayout.Height(height));
                foreach (var local in localizedStrings) {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel(local.Key);
                    if (GUILayout.Button(local.Value[selectedLanguage], "CN CountBadge")) {
                        _locKeyString = local.Key;
                        GUIUtility.hotControl = 0;
                        GUIUtility.keyboardControl = 0;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndScrollView();
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();
        }

        private void AddUnlocalizableTextTagIfNeeded() {
            // Open tag manager
            SerializedObject tagManager =
                new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            SerializedProperty tagsProp = tagManager.FindProperty("tags");

            // First check if it is not already present
            bool found = false;
            for (int i = 0; i < tagsProp.arraySize; i++) {
                SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
                if (t.stringValue.Equals(UNLOCALIZABLE_TAG)) {
                    found = true;
                    break;
                }
            }

            // if not found, add it
            if (!found) {
                tagsProp.InsertArrayElementAtIndex(0);
                SerializedProperty n = tagsProp.GetArrayElementAtIndex(0);
                n.stringValue = UNLOCALIZABLE_TAG;
            }

            // and to save the changes
            tagManager.ApplyModifiedProperties();
        }
    }
}