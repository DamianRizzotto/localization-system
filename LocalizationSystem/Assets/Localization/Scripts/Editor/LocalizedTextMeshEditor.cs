﻿using UnityEditor;

namespace Localization {
    [CustomEditor(typeof(LocalizedTextMesh))]
    [CanEditMultipleObjects]
    public class LocalizedTextMeshEditor : LocalizedEditor<LocalizedTextMesh> {
        public override void OnInspectorGUI() {
            OnInspectorGUI("key");
        }
    }
}