using System;
using UnityEditor;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using System.Linq;
using UnityQuickSheet;

namespace Localization {
    [CustomEditor(typeof(Localization))]
    public class LocalizationInspector : Editor {
        
        private const string MenuItemPath = "Localization/";
        private const string LocalizationAssetName = "Localization";
        private const string LocalizationAssetPath = "Assets/Localization/Resources/" + LocalizationAssetName + ".asset";

        [MenuItem(MenuItemPath + "Configuration", false, 0)]
        public static void GetConfiguration() {
            var asset = Resources.Load<Localization>(LocalizationAssetName);
            if (asset == null) {
                asset = CreateInstance<Localization>();
                var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(LocalizationAssetPath);
                AssetDatabase.CreateAsset(asset, assetPathAndName);
            }

            Selection.activeObject = asset;
        }

        public override void OnInspectorGUI() {
            EditorGUILayout.LabelField("Localization Settings", (GUIStyle) "IN TitleText");
            if (DrawDefaultInspector()) {
                var manager = target as Localization;
                if (manager != null) {
                    manager.InvokeOnLocalize();
                }
            }
        }

        [MenuItem(MenuItemPath + "Update Localization sheets", false, 1)]
        public static void UpdateSheets() {
            foreach (GoogleMachine machine in Localization.Instance.InputFiles) {
                machine.Import();
            }
            Localization.RebuildLocKeysEnum();
        }
    }
}