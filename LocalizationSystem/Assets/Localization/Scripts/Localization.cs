﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using ArabicSupport;
using UnityEngine.Events;
using UnityQuickSheet;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

namespace Localization {
    public enum Language {
        EN,
        ES,
        IT,
        FR,
        AR,
        GR,
        KR,
        CH,
        JP,
        PT,
        RU
    }

    [CreateAssetMenu(fileName = "Localization.asset", menuName = "Localization/Create Localization asset")]
    public class Localization : ScriptableObject {
        private const string KEY_NOT_FOUND = "[{0}]";

        [Tooltip("Text files to get localization strings from\nThese are prioritized, so the ones added later are always prioritized.")]
        
        [SerializeField]
        private List<GoogleMachine> _inputFiles;

        public List<GoogleMachine> InputFiles => _inputFiles;

        /// <summary>
        /// The singleton instance of this manager.
        /// </summary>
        private static Localization instance;
        public static Localization Instance {
            get {
                if (!HasInstance) {
                    Debug.LogError("Could not load Localization Settings from Resources");
                }

                return instance;
            }
        }

        private static bool HasInstance {
            get {
                if (instance == null) {
                    instance = (Localization) Resources.Load("Localization", typeof(Localization));
                }
                return instance != null;
            }
        }

        [Tooltip("The supported languages by the game.\n Leave empty if you support them all.")]
        public List<Language> supportedLanguages;

        [Tooltip(
            "The currently selected language of the game.\nThis will also be the default when you start the game for the first time.")]
        [SerializeField]
        private Language selectedLanguage = Language.EN;

        [Tooltip("If we cant find the string for the selected language we fall back to this language.")]
        [SerializeField]
        private Language fallbackLanguage = Language.EN;

        private readonly UnityEvent _localize = new UnityEvent();

        public Language SelectedLanguage {
            get => selectedLanguage;
            private set {
                if (IsLanguageSupported(value)) {
                    selectedLanguage = value;
                    InvokeOnLocalize();
                } else {
                    Debug.LogWarning($"{value} is not a supported language.");
                }
            }
        }

        private bool IsLanguageSupported(Language language) {
            return supportedLanguages == null || supportedLanguages.Count == 0 || supportedLanguages.Contains(language);
        }

        public void InvokeOnLocalize() {
            _localize?.Invoke();
        }

        /// <summary>
        /// Select a language, used by dropdowns and the like.
        /// </summary>
        /// <param name="language"></param>
        public void SelectLanguage(Language language) {
            SelectedLanguage = language;
        }

        /// <summary>
        /// Add a Localization listener to catch the event that is invoked when the selected language is changed.
        /// </summary>
        /// <param name="localizable"></param>
        public void AddOnLocalizeEvent(ILocalizable localizable) {
            _localize.RemoveListener(localizable.OnLocalize);
            _localize.AddListener(localizable.OnLocalize);
            localizable.OnLocalize();
        }

        /// <summary>
        /// Removes a Localization listener.
        /// </summary>
        /// <param name="localizable"></param>
        public void RemoveOnLocalizeEvent(ILocalizable localizable) {
            _localize.RemoveListener(localizable.OnLocalize);
        }

        /// <summary>
        /// Retrieves the correct language string by key.
        /// </summary>
        /// <param name="locKey">The localization key</param>
        /// <returns>A localized string</returns>
        public static string LocalizeKey(LocKey locKey) {
            var languages = LocalizationImporter.GetLanguages(locKey);
            var selected = (int) Instance.selectedLanguage;
            if (languages.Count > 0 && Instance.selectedLanguage >= 0 && selected < languages.Count) {
                var currentString = languages[selected];
                if (string.IsNullOrEmpty(currentString) || LocalizationImporter.IsLineBreak(currentString)) {
                    Debug.LogWarning(
                        $"Could not find key {locKey} for current language {Instance.selectedLanguage}. Falling back to {Instance.fallbackLanguage} with {languages[(int) Instance.fallbackLanguage]}");
                    currentString = languages[(int) Instance.fallbackLanguage];
                }

                return Instance.SelectedLanguage == Language.AR ? FixArabic(currentString) : currentString;
            }

            return GetKeyNoFoundString(locKey);
        }

        public static bool KeyExist(string key) {
            if (!Enum.TryParse(key, out LocKey locKey)) {
                return false;
            }

            var languages = LocalizationImporter.GetLanguages(locKey);
            var selected = (int) Instance.selectedLanguage;
            return languages.Count > 0 && Instance.selectedLanguage >= 0 && selected < languages.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static string GetFormat(LocKey key, params object[] arguments) {
            if (arguments == null || arguments.Length == 0) {
                return GetKeyNoFoundString(key);
            }

            return string.Format(LocalizeKey(key), arguments);
        }

        public static string GetKeyNoFoundString(object key) {
            return string.Format(KEY_NOT_FOUND, key);
        }

#if UNITY_EDITOR

        public static void RebuildLocKeysEnum() {
            LocalizationImporter.PopulateLanguageStrings();

            string enumName = "LocKey";

            MonoScript ms = MonoScript.FromScriptableObject(Instance);
            string path = AssetDatabase.GetAssetPath(ms);
            string filePathAndName = $"{Path.GetDirectoryName(path)}/{enumName}.cs";

            using (StreamWriter streamWriter = new StreamWriter(filePathAndName)) {
                streamWriter.WriteLine($"public enum {enumName} {{");
                foreach (string key in LocalizationImporter.GetKeys()) {
                    if (!key.Contains(" ")) {
                        streamWriter.WriteLine("\t" + key + ",");
                    }
                }

                streamWriter.WriteLine("}");
            }

            AssetDatabase.Refresh();
        }

#endif

        public static string FixArabic(string value) {
            try {
                string pattern = @"<(.*?)>";

                int depth = 0;
                bool closingTag = false;
                List<string> localizedSubTags = new List<string>();
                List<string> depthTexts = new List<string>()
                    {string.Empty, string.Empty, string.Empty, string.Empty, string.Empty};

                //Arabic Fixer by default will invert inequality symbols, so replace every set of <tag> content </tag>
                //they can can be used later to access localizedSubTags with the localized content and untouched tags
                for (var index = 0; index < value.Length; index++) {
                    char c = value[index];
                    if (c == '<') {
                        closingTag = value[index + 1] == '/';
                        if (!closingTag) {
                            depth++;
                        }
                    }

                    depthTexts[depth] += c;

                    if (c == '>' && closingTag) {
                        int fromIndex = GetNthIndex(depthTexts[depth], '>', 1);
                        int toIndex = GetNthIndex(depthTexts[depth], '<', 2);
                        string replaceString = depthTexts[depth].Substring(fromIndex, toIndex - fromIndex + 1);
                        if (IsArabic(replaceString)) {
                            replaceString = ArabicFixer.Fix(replaceString, false, false);
                        }

                        string finalString = depthTexts[depth].Substring(0, fromIndex + 1) + replaceString +
                                             depthTexts[depth].Substring(toIndex, depthTexts[depth].Length - toIndex);

                        depthTexts[depth] = string.Empty;
                        depth--;
                        depthTexts[depth] += "{" + localizedSubTags.Count + "}";
                        localizedSubTags.Add(finalString);
                    }
                }

                //Final cleaning of all remaining tags (single tags will be here for example) 
                while (depth != 0) {
                    int toIndex = GetNthIndex(depthTexts[depth], '>', 1);
                    if (toIndex != -1) {
                        int fromIndex = GetNthIndex(depthTexts[depth], '>', 1) + 1;
                        string replaceString =
                            depthTexts[depth].Substring(fromIndex, depthTexts[depth].Length - fromIndex);
                        if (IsArabic(replaceString)) {
                            replaceString = ArabicFixer.Fix(replaceString, false, false);
                        }

                        string finalString = depthTexts[depth].Substring(0, toIndex + 1) + replaceString;

                        depthTexts[depth - 1] += "{" + localizedSubTags.Count + "}";
                        localizedSubTags.Add(finalString);
                    }

                    depthTexts[depth] = string.Empty;
                    depth--;
                }

                if (IsArabic(depthTexts[0])) {
                    depthTexts[0] = ArabicFixer.Fix(depthTexts[0], false, false);
                }

                //Replacing keys with fixed tags
                bool replacingText = true;
                string text = depthTexts[0];
                while (replacingText) {
                    int fromIndex = GetNthIndex(text, '{', 1);
                    int toIndex = GetNthIndex(text, '}', 1);

                    if (fromIndex != -1 && toIndex != -1) {
                        int index = int.Parse(text.Substring(fromIndex + 1, toIndex - fromIndex - 1));
                        text = text.Substring(0, fromIndex) + localizedSubTags[index] +
                               text.Substring(toIndex + 1, text.Length - toIndex - 1);
                    } else {
                        replacingText = false;
                    }
                }

                return text;
            } catch (Exception e) {
                Debug.LogError($"Error fixing arab: {value}");
                return "";
            }
        }

        private static bool IsArabic(string str) {
            char[] charArray = str.ToCharArray();
            foreach (GeneralArabicLetters letter in Enum.GetValues(typeof(GeneralArabicLetters))) {
                char arabicCharacter = (char) letter;
                if (charArray.Any(character => character == arabicCharacter)) {
                    return true;
                }
            }

            return false;
        }

        private static int GetNthIndex(string s, char t, int n) {
            int count = 0;
            for (int i = 0; i < s.Length; i++) {
                if (s[i] == t) {
                    count++;
                    if (count == n) {
                        return i;
                    }
                }
            }

            return -1;
        }
    }
}