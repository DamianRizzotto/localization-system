﻿using TMPro;
using UnityEngine;

namespace Localization {
    public class LocalizationExampleController : MonoBehaviour, ILocalizable {
        [SerializeField] private TextMeshProUGUI _localizedLabel;

        private void OnEnable() {
            Localization.Instance.AddOnLocalizeEvent(this);
        }

        private void OnDisable() {
            Localization.Instance.RemoveOnLocalizeEvent(this);
        }

        public void OnLocalize() {
            _localizedLabel.text = Localization.LocalizeKey(LocKey.SCRIPT_ASSIGNED_KEY);
        }

        #region UI Events

        public void ButtonClick_Previous() {
            Language language = Localization.Instance.SelectedLanguage;
            do {
                language = language.Previous();
            } while (!Localization.Instance.supportedLanguages.Contains(language));

            Localization.Instance.SelectLanguage(language);
        }

        public void ButtonClick_Next() {
            Language language = Localization.Instance.SelectedLanguage;
            do {
                language = language.Next();
            } while (!Localization.Instance.supportedLanguages.Contains(language));

            Localization.Instance.SelectLanguage(language);
        }

        #endregion
    }
}