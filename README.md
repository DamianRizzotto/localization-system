# Localization system
Version 0.1.1

Easy to use customizable localization system for Unity.

## Features

 - Based on Google Sheets files for easy editing.
 - Localization file can be pulled from the client with just one click.
 - Creates a LocKey enum which allows for pre runtime verifications and reduces mistakes.
 - Has custom editors for easy localization key search.
 - Supports arabic languages.
 - Tests to check for unlocalized objects in scenes/prefabs.

## Getting Started

### Prerequisites
This project depends on TextMeshPro, both for it's example as well as some scripts and tools, however removing everything textMeshPro related should leave a working system (albeit with less features).

Compatibility with old versions of unity ( < 2018 ) is not guaranteed.

### 1. Initial setup

1. Install the latest .unityPackage from the download section.
2. Clone your own localization sheet based on [this one](https://docs.google.com/spreadsheets/d/1LlB4TgoqKLFNSdn7oDLKxuLY8_E_NcDhcLgYKsBqboA/edit#gid=296134756). 

### 2. Setting up OAuth2 for accessing Google Drive

To access google drive we'll need OAuth2 authentication. To set this up visit http://console.developers.google.com/ and create a new project.

![Oauth1](Images/OAuth1.png)

Select 1) Credentials on the left panel and Credential tab on the top menu then click 2) Create credentials button which open a dialogue shows credential types you can create.

Note that you should select 3) OAuth client ID otherwise you may have wrong format of json file.

![Oauth2](Images/OAuth2.png)

Select 1) othter for Application type and 2) insert a proper name into the Name field, then press 3) 'Create' button will create a credential.

![Oauth3](Images/OAuth3.png)

Now you can see 'client_ID' and 'client_secret' for newly created credential. 

![Oauth4](Images/OAuth4.png)

Finally, click 1) the download icon for downloading json file which contains 'client_ID' and 'client_secret'.

We'll now configure the "Google Data Oauth2 Settings" file. We can directly copy and paste those 'client_ID' and 'client_secret' from the previous step or use the JSON file.

![Oauth5](Images/OAuth5.png)

After filling both fields click the "Start Authentication" button. That will open a URL in which we have to give the app we just created permissions to see our files.

After getting the access code and filling the field click "Finish Authentication". If everything works well the screen should look like this:

![Oauth6](Images/OAuth6.png)

### 3. Downloading a file

Once the OAuth2 authentication is working we can go ahead and create a Google Spreadsheet Settings file via Assets > Create > Localization > Google Spreadhseet Import Settings

We should see a screen with all our different sheets, ordered by update date.

![Sheet1](Images/Sheet1.png)

After clicking on the sheet we want we can go ahead and select which worksheet we're interested in

![Sheet2](Images/Sheet2.png)

And that's it! The file is now downloaded and ready to use. The latest edit date of the file and the amount of rows will be shown.

![Sheet3](Images/Sheet3.png)

Note: Clicking "Import" will indeed download the file, but it won't trigger a regeneration of the LocKey file.

### 4. Global configuration
Going through Localization/Configurate will open a scriptable object where all the Localization System configuration is stored.

![Localization](Images/Localization.png)

 - **Input Files**
 
All the "Google Spreadhseet Import Settings" files being used for localization.

 - **Supported Languages**
 
This is a list of supported languages. If empty the system assumes all languages in the Language enum are valid.
 
  - **Fallback Language**
  
If any given key is empty in the current selected language it will fall back to this one.

### 4. Usage

Pulling sheets and generating the LocKey file.

> Fill the "Input Files" list in the Localization asset, then click Localization > Update localization sheets. That will update all files and regenerate the LocKey enum. 

Getting a localized string.

> Localization.LocalizeKey(LocKey)

Setting the current language

> Localization.SetSelectedLanguage(Language)

Fix arabic texts (done by default if the language is AR, but should be used to fix all player input)

> Localization.FixArabic(string)

## Utils included

 - **Localization scripts with custom editors**
 
Custom editor to quickly search for localization keys

 ![Custom editor](Images/CustomEditor.png)
 
 - **Localization checker**
 
Checks if every TextMeshProUGUI object in the current open scene/prefab has it's labels localized. For localizable texts set on runtime the system provides a way to tag them quickly with "UnlocalizableText".

 ![Localization checker](Images/Checker.png)
 
 - **Localization key search**
  
Quickly searches for the first TextMeshProUGUI object in the current open scene/prefab with a given LocalizationKey in it's localization component.

 ![Search for keys](Images/Search.png)
 
 - **Arabic support tool**
 
Quick way to test if the arabic fixer system is behaving correctly.

 ![Arabic tool](Images/Arabic.png)
 
## Future improvements

 - Re enable support to export directly to android folder (For example, for push notifications)
 - Ability to load additional localization files on runtime
 - Build pipeline integration (Pull & Optimization of unused keys)

## Troubleshooting
I'm setting a language and getting another

> Check that the columns on the google sheets file are in the same order as the Localization enum

When downloading the mastersheet the file being downloaded is an HTML webpage

> Check that the google sheets file is set to Public (at least for viewing with the link)
 
When downloading the mastersheet the LocKey file doesn't update

> It's most likely a configuration issue, check if the format of the mastersheet being downloaded is the same as the one under Input Files

The language I need is not in the list
> You can add languages by editing the Language enum in Localization.cs, remember to add a case on SaveLanguagePreference.cs so it can choose it based on the device language.